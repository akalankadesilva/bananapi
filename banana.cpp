#include "banana.h"

void banana::backupWrite(int i, uint32_t v) {
  BKP_REG_BASE[2*i]= (v << 16);
  BKP_REG_BASE[2*i+1]= (v & 0xFFFF);
}

uint32_t banana::backupRead(int i) {
  return ((BKP_REG_BASE[2*i] & 0xFFFF) >> 16) | (BKP_REG_BASE[2*i+1] & 0xFFFF);
}


void banana::standbyMode(){
 
  PWR_BASE->CR |= PWR_CR_PDDS;
  PWR_BASE->CR |= PWR_CR_LPDS;
   SCB_BASE->SCR |= SCB_SCR_SLEEPDEEP;
    asm("    wfi"); 
}


void banana::sleepMode() {
    PWR_BASE->CR &= ~PWR_CR_PDDS;
    PWR_BASE->CR &= ~PWR_CR_LPDS;
	PWR_BASE->CR |= PWR_CR_CWUF;
	PWR_BASE->CR |= PWR_CR_LPDS;
	SCB_BASE->SCR |= SCB_SCR_SLEEPDEEP;
	asm("    wfi");  
}

void banana::restart() {
   nvic_sys_reset();
}



void banana::disableAllClocks(){
	rcc_clk_disable(RCC_ADC1);
	rcc_clk_disable(RCC_ADC2);
	rcc_clk_disable(RCC_ADC3);
	rcc_clk_disable(RCC_AFIO);
	rcc_clk_disable(RCC_BKP);
	rcc_clk_disable(RCC_CRC);
	rcc_clk_disable(RCC_DAC);
	rcc_clk_disable(RCC_DMA1);
	rcc_clk_disable(RCC_DMA2);
	rcc_clk_disable(RCC_FLITF);
	rcc_clk_disable(RCC_FSMC);
	rcc_clk_disable(RCC_GPIOE);
	rcc_clk_disable(RCC_GPIOF);
	rcc_clk_disable(RCC_GPIOG);
	rcc_clk_disable(RCC_I2C1);
	rcc_clk_disable(RCC_I2C2);
	rcc_clk_disable(RCC_PWR);
	rcc_clk_disable(RCC_SDIO);
	rcc_clk_disable(RCC_SPI1);
	rcc_clk_disable(RCC_SPI2);
	rcc_clk_disable(RCC_SPI3);
	rcc_clk_disable(RCC_SRAM);
	rcc_clk_disable(RCC_TIMER2);
	rcc_clk_disable(RCC_TIMER3);
	rcc_clk_disable(RCC_TIMER4);
	rcc_clk_disable(RCC_TIMER5);
	rcc_clk_disable(RCC_TIMER6);
	rcc_clk_disable(RCC_TIMER7);
	rcc_clk_disable(RCC_TIMER8);
	rcc_clk_disable(RCC_TIMER9);
	rcc_clk_disable(RCC_TIMER10);
	rcc_clk_disable(RCC_TIMER11);
	rcc_clk_disable(RCC_TIMER12);
	rcc_clk_disable(RCC_TIMER13);
	rcc_clk_disable(RCC_TIMER14);
	rcc_clk_disable(RCC_USART1);
	rcc_clk_disable(RCC_USART2);
	rcc_clk_disable(RCC_USART3);
	rcc_clk_disable(RCC_UART4);
	rcc_clk_disable(RCC_UART5);
	rcc_clk_disable(RCC_USB);
}
