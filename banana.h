#include <libmaple/pwr.h>
#include <libmaple/rcc.h>
#include <libmaple/scb.h>
#include <libmaple/nvic.h>

#define BKP_REG_BASE   ((uint32_t *)(0x40006C00 + 0x04))

class banana{
public:
	void standbyMode();
	void sleepMode();
	void restart();
	void backupWrite(int i, uint32_t v);
	uint32_t backupRead(int i);
	void disableAllClocks();

};
